import React, { useState, useEffect } from "react"
import { Link } from 'react-router-dom'

import logoImg from '../../assets/images/logo.svg'
import landingImg from '../../assets/images/landing.svg'
import studyIcon from '../../assets/images/icons/study.svg'
import giveClassesIcon from '../../assets/images/icons/give-classes.svg'
import purpleHeartIcon from '../../assets/images/icons/purple-heart.svg'
import api from "../../services/api"

import './styles.css'


function Landing() {
    const [totalConnections, setTotalConnections] = useState(0)

    useEffect(() => {
        api.get('connections').then(response => {
            const {total} = response.data

            setTotalConnections(total)
        })
    },  [])

    return (
        <div id="page-landing">
            <div id="page-landing-content" className="conteiner">
                <div className="logo-conteiner">
                    <img src={logoImg} alt="proffy"/>
                    <h2>Sua plataforma de estudo online</h2>
                </div>

                <img src={landingImg} alt="platform of studt=y"  className="hero-image"/>

                <div className="buttons-conteiner">
                    <Link to="/study" className="study">
                        <img src={studyIcon} alt="study"/>
                        Estudar
                    </Link>

                    <Link to="/give-classes" className="give-classes">
                        <img src={giveClassesIcon} alt="give classes"/>
                        Dar Aulas
                    </Link>

                </div>

                <span className="total-connections" >
                    total de {totalConnections} conexões já realizadas <img src={purpleHeartIcon} alt=""/>
                </span>
            </div>
        </div>
    )
}

export default Landing;