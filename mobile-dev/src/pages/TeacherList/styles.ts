import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f0f0f7',
    } ,
    teacherList: {
        marginTop: -40,
    },
    searchForm: {
        marginBottom: 8
    },
    label: {
        color: '#d4c2ff'
    },
    input:{
        height: 34,
        backgroundColor: '#fff',
        borderRadius: 8,
        justifyContent: 'center',
        paddingHorizontal: 16,
        marginTop: 4,
        marginBottom: 8
    },
    inputGroup: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    inputBlock: {
        width: '48%'
    },
    filterButton: {
        backgroundColor: '#d4c2ff',
        padding: 10,
        borderRadius: 50,
        borderColor: '#eee',
    },
    submitButton: {
        backgroundColor: '#04d361',
        height: 35,
        borderRadius: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    submitButtonText: {
       color: '#fff',
    } 
})

export default styles
